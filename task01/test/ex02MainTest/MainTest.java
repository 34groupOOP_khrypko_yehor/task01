package ex02MainTest;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import ex01.Item2d;
import ex02.ViewResult;
/** ��������� ������������
* ������������� �������.
* @author xone
* @version 2.0
*/
public class MainTest {
/** �������� �������� ���������������� ������ {@linkplain ViewResult} */
@Test
public void testCalc() {
ViewResult view = new ViewResult(5);
view.init(90.0);
Item2d item = new Item2d();
int ctr = 0;
item.setXY(0.0, 0.0);
assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
view.getItems().get(ctr).equals(item));
ctr++;
item.setXY(90.0, 1.0);
assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
view.getItems().get(ctr).equals(item));
ctr++;
item.setXY(180.0, 0.0);
assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
view.getItems().get(ctr).equals(item));
ctr++;
item.setXY(270.0, -1.0);
assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
view.getItems().get(ctr).equals(item));
ctr++;
item.setXY(360.0, 0.0);
assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
view.getItems().get(ctr).equals(item));
}
/** �������� ������������. ������������ �������������� ������. */
@Test
public void testRestore() {
ViewResult view1 = new ViewResult(1000);
ViewResult view2 = new ViewResult();
// �������� �������� ������� �� ��������� ����� ���������� ���������
view1.init(Math.random()*100.0);
// �������� ��������� view1.items
try {
view1.viewSave();
} catch (IOException e) {
e.getMessage();
}
// �������� ��������� view2.items
try {
view2.viewRestore();
} catch (Exception e) {
e.getMessage();
}
// ������ ��������� ������� �� ���������, ������� ���������
assertEquals(view1.getItems().size(), view2.getItems().size());
// ������ ��� �������� ������ ���� �����.
// ��� ����� ����� ���������� ����� equals
assertTrue("containsAll()", view1.getItems().containsAll(view2.getItems()));
}
}