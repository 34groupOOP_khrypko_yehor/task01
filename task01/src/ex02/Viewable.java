package ex02;
/** Creator
* (������ ��������������
* Factory Method)<br>
* ��������� �����,
* "�����������" �������
* @author xone
* @version 1.0
* @see Viewable#getView()
*/
public interface Viewable {
/** ������ ������, ����������� {@linkplain View} */
public View getView();
}