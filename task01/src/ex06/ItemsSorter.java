package ex06;
import java.util.Collections;
/** �����������;
* ���������� ������
* ��������� �������;
* ���������� Event;
* ������ Observer
* @author xone
* @see AnnotatedObserver
* @see Event
*/
public class ItemsSorter extends AnnotatedObserver {
/** ���������-������������� �������, ��������������� ������������� */
public static final String ITEMS_SORTED = "ITEMS_SORTED";
/** ���������� ������� {@linkplain Items#ITEMS_CHANGED};
* �������� ������������; ������ Observer
* @param observable ����������� ������ ������ {@linkplain Items}
* @see Observable
*/
@Event(Items.ITEMS_CHANGED)
public void itemsChanged(Items observable) {
Collections.sort(observable.getItems());
observable.call(ITEMS_SORTED);
}
/** ���������� ������� {@linkplain Items#ITEMS_SORTED}; ������ Observer
* @param observable ����������� ������ ������ {@linkplain Items}
* @see Observable
*/
@Event(ITEMS_SORTED)
public void itemsSorted(Items observable) {
System.out.println(observable.getItems());
}
/** ���������� ������� {@linkplain Items#ITEMS_REMOVED}; ������ Observer
* @param observable ����������� ������ ������ {@linkplain Items}
* @see Observable
*/
@Event(Items.ITEMS_REMOVED)
public void itemsRemoved(Items observable) {
System.out.println(observable.getItems());
}
}