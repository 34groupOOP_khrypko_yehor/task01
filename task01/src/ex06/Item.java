package ex06;
/** ���������� �������
* ���������
* @author xone
* @see Items
*/
public class Item implements Comparable<Item> {
/** �������������� ���� */
private String data;
/** �������������� {@linkplain Item#data}
* @param data �������� ��� ���� {@linkplain Item#data}
*/
public Item(String data) {
this.data = data;
}
/** ������������� ���� {@linkplain Item#data}
* @param data �������� ��� ���� {@linkplain Item#data}
* @return �������� ���� {@linkplain Item#data}
*/
public String setData(String data) {
return this.data = data;
}
/** ���������� ���� {@linkplain Item#data}
* @return �������� ���� {@linkplain Item#data}
*/
public String getData() {
return data;
}
@Override
public int compareTo(Item o) {
return data.compareTo(o.data);
}
@Override
public String toString() {
return data;
}
}