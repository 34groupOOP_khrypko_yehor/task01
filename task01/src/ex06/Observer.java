package ex06;
/** ������������ �����
* ��� ��������������
* ������������ �������
* � �����������;
* ������ Observer
* @author xone
* @version 1.0
* @see Observable
*/
public interface Observer {
/** ���������� ����������� �������� ��� ������� �����������; ������ Observer
* @param observable ������ �� ����������� ������
* @param event ���������� � �������
*/
public void handleEvent(Observable observable, Object event);
}