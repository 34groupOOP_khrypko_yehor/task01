package ex06;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/** ��������� ��������;
* ����������� ������;
* ������ Observer
* @author xone
* @see Observable
* @see Observer
* @see Item
*/
public class Items extends Observable implements Iterable<Item> {
/** ���������-������������� �������, ��������������� ������������� */
public static final String ITEMS_CHANGED = "ITEMS_CHANGED";
/** ���������-������������� �������, ��������������� ������������� */
public static final String ITEMS_EMPTY = "ITEMS_EMPTY";
/** ���������-������������� �������, ��������������� ������������� */
public static final String ITEMS_REMOVED = "ITEMS_REMOVED";
/** ��������� �������� ������ {@linkplain Item} */
private List<Item> items = new ArrayList<Item>();
/** ��������� ������ � ��������� � �������� ������������
* @param item ������ ������ {@linkplain Item}
*/
public void add(Item item) {
items.add(item);
if (item.getData().isEmpty()) call(ITEMS_EMPTY);
else call(ITEMS_CHANGED);
}
/** ��������� ������ � ���������
* @param s ���������� ������������ {@linkplain Item#Item(String)}
*/
public void add(String s) {
add(new Item(s));
}
/** ��������� ��������� �������� � ��������� � �������� ������������
* @param n ���������� ����������� �������� ������ {@linkplain Item}
*/
public void add(int n) {
if (n > 0) {
while (n-- > 0) items.add(new Item(""));
call(ITEMS_EMPTY);
}
}
/** ������� ������ �� ��������� � �������� ������������
* @param item ��������� ������
*/
public void del(Item item) {
if (item != null) {
items.remove(item);
call(ITEMS_REMOVED);
}
}
/** ������� ������ �� ��������� � �������� ������������
* @param index ������ ���������� �������
*/
public void del(int index) {
if ((index >= 0) && (index < items.size())) {
items.remove(index);
call(ITEMS_REMOVED);
}
}
/** ���������� ������ �� ���������
* @return ������ �� ��������� �������� ������ {@linkplain Item}
*/
public List<Item> getItems() {
return items;
}
@Override
public Iterator<Item> iterator() {
return items.iterator();
}
}